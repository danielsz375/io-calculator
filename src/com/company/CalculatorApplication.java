package com.company;

import com.company.MyButtons.*;

import javax.swing.*;
import java.awt.*;

public class CalculatorApplication {

    private JFrame frame;
    private CalculatorData calculatorData = new CalculatorData();
    private ResultField resultField = new ResultField();
    private final MyButton[] buttons = new MyButton[19];

    public CalculatorApplication() {
        frame = new JFrame("CalculatorApplication");
        frame.setSize(new Dimension(230, 242));
        frame.setResizable(false);

        //Setting content of JFrame
        setResultField();
        setButtons();

        frame.setLocationByPlatform(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void setResultField() {
        resultField.setText("0");
        resultField.setBackground(Color.WHITE);
        resultField.setEnabled(false);
        resultField.setHorizontalAlignment(4);
        resultField.setDisabledTextColor(Color.BLACK);

        final int textPanelLocation_X = 12;
        final int textPanelLocation_Y = 12;
        final int textPanelWidth = 190;
        final int textPanelHeight = 50;

        resultField.setBounds(textPanelLocation_X, textPanelLocation_Y, textPanelWidth, textPanelHeight);
        frame.add(resultField);
    }

    private void setButtons() {
        NumberButton[] numberButtons = new NumberButton[10];
        for ( int i = 9; i >= 0; i--) {
            numberButtons[i] = new NumberButton(Integer.toString(i), resultField, calculatorData);
        }

        MyButton addButton = new AddOperationButton("+", resultField, calculatorData);
        MyButton subtractButton = new SubtractOperationButton("-", resultField, calculatorData);
        MyButton multiplyButton = new MultiplyOperationButton("*", resultField, calculatorData);
        MyButton divideButton = new DivideOperationButton("/", resultField, calculatorData);
        MyButton clearButton = new ClearButton("C", resultField, calculatorData);
        MyButton calculateResultButton = new CalculateResultButton("=", resultField, calculatorData);

        MyButton decimalSeparatorButton = new DecimalSeparatorButton(",", resultField);
        MyButton plusMinusButton = new PlusMinusButton("±", resultField);

        MyButton offButton = new OffButton("OFF");

        buttons[0] = numberButtons[7];
        buttons[1] = numberButtons[8];
        buttons[2] = numberButtons[9];
        buttons[3] = divideButton;
        buttons[4] = offButton;
        buttons[5] = numberButtons[4];
        buttons[6] = numberButtons[5];
        buttons[7] = numberButtons[6];
        buttons[8] = multiplyButton;
        buttons[9] = clearButton;
        buttons[10] = numberButtons[1];
        buttons[11] = numberButtons[2];
        buttons[12] = numberButtons[3];
        buttons[13] = subtractButton;
        buttons[14] = calculateResultButton;
        buttons[15] = numberButtons[0];
        buttons[16] = decimalSeparatorButton;
        buttons[17] = plusMinusButton;
        buttons[18] = addButton;

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(null);
        buttonsPanel.setPreferredSize(new Dimension(190, 123));

        //Setting buttons' attributes and adding them to buttonsPanel
        final int firstButtonLocation_X = 12;
        final int firstButtonLocation_Y = 67;
        final int buttonWidth = 34;
        final int buttonHeight = 27;

        int j = 0, k = 0;
        for(int i = 0; i < 19; i++) {
            buttonsPanel.add(buttons[i]);
            buttons[i].setMargin(new Insets(0, 0, 0,0));
            buttons[i].setFont(new Font("Courier New", Font.BOLD, 15));
            if(i == 14) {
                buttons[i].setBounds(firstButtonLocation_X + j * 39, firstButtonLocation_Y + k * 32, buttonWidth, 59);
            } else buttons[i].setBounds(firstButtonLocation_X + j * 39, firstButtonLocation_Y + k * 32, buttonWidth, buttonHeight);

            j++;
            if(j == 5) {
                j = 0;
                k++;
            }
        }

        frame.add(buttonsPanel);
    }

}