package com.company.TextConversion;

public class ScientificForm {

    public String convert(String text) {
        String validatedText = text;
        if(text.contains("E-")) {
            int position = text.indexOf('E');
            String exp = text.substring(position + 2);

            if(exp.equals("0")) {
                validatedText = text.substring(0, position);
            } else if(exp.length() > 4) {
                validatedText = "Przepełnienie";
            }

        } else if(text.contains("E")) {
            int position = text.indexOf('E');
            String exp = text.substring(position + 1);

            if(exp.equals("0")) {
                validatedText = text.substring(0, position);
            } else if(exp.length() > 4) {
                validatedText = "Przepełnienie";
            } else {
                validatedText = text.substring(0, position + 1) + "+" + exp;
            }

        }

        return validatedText;
    }
}
