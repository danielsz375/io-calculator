package com.company.TextConversion;

public class TextConverter {

    public String convertToDisplayForm(String text) {
        String newText = text;
        if(text.contains(".")) {
            newText = text.replace('.', ',');
        }

        if(text.contains("E")) {
            ScientificForm scientificForm = new ScientificForm();
            return scientificForm.convert(newText);
        }

        return newText;
    }

}
