package com.company.Operations;

public class AddOperation implements Operation {
    public double evaluate(double operand1, double operand2) {
        return operand1 + operand2;
    }
}
