package com.company.Operations;

public interface Operation {
    public double evaluate(double operand1, double operand2);
}
