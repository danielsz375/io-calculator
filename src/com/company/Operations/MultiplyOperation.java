package com.company.Operations;

import java.math.BigDecimal;

public class MultiplyOperation implements Operation {
    public double evaluate(double operand1, double operand2) {
        return operand1 * operand2;
    }
}
