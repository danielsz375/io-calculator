package com.company.Operations;

public class SubtractOperation implements Operation {
    public double evaluate(double operand1, double operand2) {
        return operand1 - operand2;
    }
}