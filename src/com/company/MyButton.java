package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyButton extends JButton implements ActionListener {

    protected ResultField resultField;
    protected CalculatorData calculatorData;

    public MyButton(String title) {
        super(title);
    }

    public MyButton(String title, ResultField resultField) {
        super(title);
        this.resultField = resultField;
    }

    public MyButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title);
        this.resultField = resultField;
        this.calculatorData = calculatorData;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
