package com.company;

import com.company.LengthValidation.LengthValidator;
import com.company.TextConversion.TextConverter;

import javax.swing.*;
import java.text.DecimalFormat;

public class ResultField extends JTextField {

    private final TextConverter textConverter = new TextConverter();
    private final LengthValidator lengthValidator = new LengthValidator();
    private boolean clearFlag = false;


    @Override
    public void setText(String inputText) {
        if(inputText.length() == 0) {
            inputText = "0";
        }

        if(!lengthValidator.validateLength(inputText)) {
            double parsedNumber = Double.parseDouble(inputText.replace(',', '.'));
            DecimalFormat df = new DecimalFormat("0.###############E0");
            df.setMaximumFractionDigits(15);
            inputText = df.format(parsedNumber);
        }

        String text = textConverter.convertToDisplayForm(inputText);
        super.setText(text);
    }

    @Override
    public String getText() {
        String text = super.getText().replace(',', '.');

        if(text.equals("0.")) {
            return "0.";
        }

        if(super.getText().contains("E")) {
            return String.valueOf(Double.parseDouble(text));
        }

        return text;
    }

    public boolean isThereFreeSpace() {
        return lengthValidator.validateLength(super.getText() + "0");
    }

    public void clear() {
        super.setText("0");
        return;
    }

    public void setClearFlag(boolean clearFlag) {
        this.clearFlag = clearFlag;
    }

    public boolean isClearFlag() {
        return clearFlag;
    }

}
