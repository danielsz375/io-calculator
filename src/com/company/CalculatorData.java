package com.company;

import com.company.Operations.Operation;

public class CalculatorData {
    private double operand1 = 0;
    private double operand2 = 0;
    private Operation operation;


    public double getOperand1() {
        return operand1;
    }

    public void setOperand1(double operand1) {
        this.operand1 = operand1;
    }

    public double getOperand2() {
        return operand2;
    }

    public void setOperand2(double operand2) {
        this.operand2 = operand2;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    public void resetOperation() {
        this.operation = null;
    }

    public void resetOperands() {
        this.operand1 = 0;
        this.operand2 = 0;
    }
}
