package com.company.MyButtons;

import com.company.MyButton;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class OffButton extends MyButton {

    public OffButton(String title) {
        super(title);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.exit(0);
    }
}
