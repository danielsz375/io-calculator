package com.company.MyButtons;

import com.company.Checkers.ErrorChecker;
import com.company.MyButton;
import com.company.ResultField;
import com.company.Checkers.ZeroChecker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlusMinusButton extends MyButton {


    public PlusMinusButton(String title, ResultField resultField) {
        super(title, resultField);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }

        String text = resultField.getText();
        double tempValue = Double.parseDouble(text);

        ZeroChecker zeroChecker = new ZeroChecker();
        if (!zeroChecker.isZero(tempValue)) {
            double newTempValue = (-1) * tempValue;

            if(String.valueOf(newTempValue).contains("E")) {
                text = String.valueOf(newTempValue);
            } else if(newTempValue % 1 == 0) {
                text = String.valueOf((long)newTempValue);
            } else {
                text = String.valueOf(newTempValue);
            }
            resultField.setText(text);
        }
    }
}
