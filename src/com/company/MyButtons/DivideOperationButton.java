package com.company.MyButtons;

import com.company.CalculatorData;
import com.company.Checkers.ErrorChecker;
import com.company.MyButton;
import com.company.Operations.DivideOperation;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class DivideOperationButton extends MyButton {

    public DivideOperationButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title, resultField, calculatorData);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }


        calculatorData.setOperand1(Double.parseDouble(resultField.getText()));
        calculatorData.setOperation(new DivideOperation());
        resultField.setClearFlag(true);
    }
}
