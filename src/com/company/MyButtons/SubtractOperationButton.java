package com.company.MyButtons;

import com.company.CalculatorData;
import com.company.Checkers.ErrorChecker;
import com.company.MyButton;
import com.company.Operations.SubtractOperation;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class SubtractOperationButton extends MyButton {

    public SubtractOperationButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title, resultField, calculatorData);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }

        calculatorData.setOperand1(Double.parseDouble(resultField.getText()));
        calculatorData.setOperation(new SubtractOperation());
        resultField.setClearFlag(true);
    }
}
