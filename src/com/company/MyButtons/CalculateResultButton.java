package com.company.MyButtons;

import com.company.CalculatorData;
import com.company.Checkers.ErrorChecker;
import com.company.Checkers.ZeroChecker;
import com.company.MyButton;
import com.company.Operations.DivideOperation;
import com.company.Operations.Operation;
import com.company.ResultField;

import java.awt.event.ActionEvent;
import java.math.BigDecimal;

public class CalculateResultButton extends MyButton {

    public CalculateResultButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title, resultField, calculatorData);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }

        double result = 0;
        calculatorData.setOperand2(Double.parseDouble(resultField.getText()));
        Operation operation = calculatorData.getOperation();
        double operand1 = calculatorData.getOperand1();
        double operand2 = calculatorData.getOperand2();

        if(operation == null) {
            return;
        } else {
            ZeroChecker zeroChecker = new ZeroChecker();
            if(operation instanceof DivideOperation && zeroChecker.isZero(operand2)) {
                resultField.setText("Nieprawidłowa operacja");
                return;
            } else {
                result = operation.evaluate(operand1, operand2);
                System.out.println(result);
            }
        }

        String resultAsString = String.valueOf(result);

        resultField.setText(resultAsString);
        calculatorData.setOperand2(0);
        resultField.setClearFlag(true);
        calculatorData.resetOperation();
    }
}
