package com.company.MyButtons;

import com.company.CalculatorData;
import com.company.Checkers.ErrorChecker;
import com.company.Checkers.ZeroChecker;
import com.company.MyButton;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class NumberButton extends MyButton {

    private String c;

    public NumberButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title, resultField, calculatorData);
        this.c = title;
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }

        String text = resultField.getText();
        ZeroChecker zeroChecker = new ZeroChecker();
        String textToDisplay = text + c;
        if(resultField.isClearFlag() || zeroChecker.isZero(Double.parseDouble(text)) && !text.contains(".")) {
            textToDisplay = c;
            resultField.setClearFlag(false);
        } else if(!resultField.isThereFreeSpace()) {
            return;
        }

        resultField.setText(textToDisplay);
    }
}
