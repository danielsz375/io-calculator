package com.company.MyButtons;

import com.company.Checkers.ErrorChecker;
import com.company.MyButton;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class DecimalSeparatorButton extends MyButton {

    public DecimalSeparatorButton(String title, ResultField resultField) {
        super(title, resultField);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ErrorChecker errorChecker = new ErrorChecker();
        if(errorChecker.isError(resultField)) {
            return;
        }

        String text = resultField.getText();
        String newText = text;

        if (!text.contains(".")) {
            newText = text + ".";
        } else if ((text.charAt(text.length() - 1) == '.')){
            newText = text.replace(".", "");
        }

        resultField.setText(newText);
    }
}
