package com.company.MyButtons;

import com.company.CalculatorData;
import com.company.MyButton;
import com.company.ResultField;

import java.awt.event.ActionEvent;

public class ClearButton extends MyButton {

    public ClearButton(String title, ResultField resultField, CalculatorData calculatorData) {
        super(title, resultField, calculatorData);
        addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        resultField.clear();
        resultField.setClearFlag(false);
        calculatorData.resetOperands();
    }
}
