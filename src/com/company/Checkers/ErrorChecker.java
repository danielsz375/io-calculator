package com.company.Checkers;

import com.company.ResultField;

public class ErrorChecker {

    public boolean isError(ResultField rf) {
        String text = rf.getText();
        switch(text) {
            case "Przepełnienie":
            case "Nieprawidłowa operacja":
                return true;
            default:
                return false;
        }
    }
}
