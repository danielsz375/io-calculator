package com.company.Checkers;

public class ZeroChecker {

    public boolean isZero(double value) {
        return value >= -Double.MIN_VALUE && value <= Double.MIN_VALUE;
    }
}
