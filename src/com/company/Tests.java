package com.company;

import com.company.LengthValidation.DigitsCounter;
import com.company.MyButtons.NumberButton;
import com.company.Operations.*;
import org.junit.Assert;
import org.junit.Test;

import java.awt.event.ActionEvent;

import static org.junit.Assert.*;

public class Tests {

    // Operations
    @Test
    public void shouldAddValues() {
        Operation operation = new AddOperation();
        double actualResult = operation.evaluate(2.3, 34.75);
        Assert.assertEquals(37.05, actualResult, Double.MIN_VALUE);
    }

    @Test
    public void shouldSubtractValues() {
        Operation operation = new SubtractOperation();
        double actualResult = operation.evaluate(123, 68.05);
        Assert.assertEquals(54.95, actualResult, Double.MIN_VALUE);
    }

    @Test
    public void shouldDivideValues() {
        Operation operation = new DivideOperation();
        double actualResult = operation.evaluate(50, 8);
        Assert.assertEquals(6.25, actualResult, Double.MIN_VALUE);
    }

    @Test
    public void shouldMultiplyValues() {
        Operation operation = new MultiplyOperation();
        double actualResult = operation.evaluate(2.1, 7.7);
        Assert.assertEquals(16.17, actualResult, Double.MIN_VALUE);
    }


    //DigitsCounter
    @Test public void shouldCountDigits() {
        DigitsCounter digitsCounter = new DigitsCounter();
        int actualResult = digitsCounter.countDigits("-123,456");
        Assert.assertEquals(6, actualResult);
    }

    @Test public void shouldCountDigitsScientificForm() {
        DigitsCounter digitsCounter = new DigitsCounter();
        int actualResult = digitsCounter.countDigits("-1,234567890123456E-32");
        Assert.assertEquals(16, actualResult);
    }


    //ResultField
    @Test
    public void shouldSetText() {
        ResultField resultField = new ResultField();
        resultField.setText("123");
        Assert.assertEquals("123", resultField.getText());
    }

    @Test
    public void shouldConvertCommaToDot() {
        ResultField resultField = new ResultField();
        resultField.setText("123,456");
        Assert.assertEquals("123.456", resultField.getText());
    }

    @Test
    public void shouldUseScientificNotation() {
        ResultField resultField = new ResultField();
        resultField.setText("12345678901234567890");
        Assert.assertEquals("1.234567890123457E19", resultField.getText());
    }

    @Test
    public void shouldClear() {
        ResultField resultField = new ResultField();
        resultField.setText("123");
        resultField.clear();
        Assert.assertEquals("0", resultField.getText());
    }

}