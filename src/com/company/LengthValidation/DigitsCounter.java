package com.company.LengthValidation;

public class DigitsCounter {

    public int countDigits(String str) {
        if(str.length() == 0) {
            return 0;
        }

        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'E') break;
            if (Character.isDigit(str.charAt(i))) {
                count++;
            }
        }
        return count;
    }
}
