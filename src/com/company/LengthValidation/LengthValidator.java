package com.company.LengthValidation;

public class LengthValidator {
    private final DigitsCounter digitsCounter = new DigitsCounter();


    public boolean validateLength(String text) {
        if(digitsCounter.countDigits(text) > 16) {
            return false;
        }
        return true;
    }

}
